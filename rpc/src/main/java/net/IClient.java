package net;

import sync.RpcRequest;
import sync.RpcResponse;

public interface IClient {

	/**发送请求**/
	void sendRpcRequest(RpcRequest request);
	
	/**发送返回**/
	void sendRpcResponse(RpcResponse response);
}
