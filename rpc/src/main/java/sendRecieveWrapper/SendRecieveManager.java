package sendRecieveWrapper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by JinMiao
 * 2017/4/24.
 */
public class SendRecieveManager {

    private Map<Class, ICallerResponseTypeWrapperFactory> callerReponseMap = new ConcurrentHashMap<>();

    private Map<Class, ICalledResponseTypeWrapper> calledReponseMap = new ConcurrentHashMap<>();


    public void init(){


    }



    public void register(Class returnType, ICallerResponseTypeWrapperFactory callerResponseTypeWrapperFactory)
    {
        callerReponseMap.put(returnType,callerResponseTypeWrapperFactory);
    }


    public void register(Class returnType, ICalledResponseTypeWrapper callerResponseTypeWrapperFactory)
    {
        calledReponseMap.put(returnType,callerResponseTypeWrapperFactory);
    }


    public ICalledResponseTypeWrapper getICalledResponseTypeWrapper(Class returnType) {
        return calledReponseMap.get(returnType);
    }
    public ICallerResponseTypeWrapperFactory getICallerResponseTypeWrapperFactory(Class returnType) {
        return callerReponseMap.get(returnType);
    }
}
