package sendRecieveWrapper;

/**
 * Created by JinMiao
 * 2017/4/21.
 */
public interface ICallerResponseTypeWrapperFactory {

    ICallerResponseTypeWrapper createRpcHandler();
}
