package sendRecieveWrapper;

/**
 * Created by JinMiao
 * 2017/4/21.
 */
public interface ICallerResponseTypeWrapper<T> {

    T afterSend();

    void afterRecieve(Object responseBody);

    void onExecption(Throwable throwable);



}
