package sendRecieveWrapper;

import sync.RpcResponse;

/**
 * Created by JinMiao
 * 2017/4/24.
 */
public interface ICalledResponseTypeWrapper<T> {
    RpcResponse afterRecieve(T returnVal);
}
