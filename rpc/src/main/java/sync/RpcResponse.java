package sync;

public class RpcResponse {
	/**回调id**/
	private int responseId;
	/**回调消息体**/
	private Object responseBody;

	private Throwable throwable;

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public int getResponseId() {
		return responseId;
	}
	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}

	public Object getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(Object responseBody) {
		this.responseBody = responseBody;
	}

	@Override
	public String toString() {
		return "RpcResponse{" +
				"responseId=" + responseId +
				", responseBody=" + responseBody +
				", throwable=" + throwable +
				'}';
	}
}

