package sync;

public class RpcRequest
{
	private String name;
	
    private Object[] arguments;
    /**请求id**/
    private Integer requestId;
    /**协议版本号**/
    private int protocolVersion;
    
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object[] getArguments() {
		return arguments;
	}
	public int getProtocolVersion() {
		return protocolVersion;
	}
	public void setArguments(Object[] arguments) {
		this.arguments = arguments;
	}
	public Integer getRequestId() {
		return requestId;
	}
	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	public void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
}
