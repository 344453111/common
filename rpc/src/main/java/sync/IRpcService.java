package sync;

public interface IRpcService
{
	/**
	 * 调用rpc服务
	 * @param cls
	 * @return
	 */
	<T> T rpcService(Class<T> cls);
	
	
}
