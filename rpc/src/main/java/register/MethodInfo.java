package register;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicInteger;

public class MethodInfo
{
    private static final AtomicInteger ATOMIC_INTEGER =  new AtomicInteger();
	private Object target;
	private Method method;
	private boolean needReturn = true;
	private String requestName;
	private int methodId;
	private Class returnType;



	public MethodInfo(Method method,Object target)
	{
	    this.method = method;
        this.target = target;
        Class<?> returnType = method.getReturnType();
        this.returnType = returnType;
	}


    public MethodInfo(Method method) {
        this.method = method;
		Class<?> returnType = method.getReturnType();
        this.returnType = returnType;
		this.needReturn = returnType!=Void.TYPE;
        this.methodId = ATOMIC_INTEGER.addAndGet(1);
        Class<?> interfaceClass = method.getDeclaringClass();
        StringBuilder sb = new StringBuilder();
        sb.append(interfaceClass.getName());
        sb.append(method.getName());
        for(Class<?> parameterTypeClas:method.getParameterTypes())
        {
            sb.append(parameterTypeClas.getName());
        }
        this.requestName = sb.toString();
	}

	public Object invoke(Object[] paramArray) throws Throwable
	{
        return method.invoke(target, paramArray);
	}


    public boolean isNeedReturn() {
		return needReturn;
	}

    public String getRequestName() {
        return requestName;
    }


    public Class getReturnType() {
        return returnType;
    }
}
