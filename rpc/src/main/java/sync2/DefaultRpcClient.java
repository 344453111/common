package sync2;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class DefaultRpcClient implements IRpcClient{

	/**
	 * 回调列表
	 */
	private List<Response> responseList = new LinkedList<>();
	
	@Override
	public void send(Object request) {
		
	}

	@Override
	public <T> T send(Object request, int timeOut) {
		return null;
	}
	
	

	@Override
	public <T> void send(Object request, Consumer<T> backFunc, Consumer<Throwable> error, Executor executor) {

		
	}

	@Override
	public <T> void send(Object request, Consumer<T> backFunc, Consumer<Throwable> error, Executor executor,
			int timeOut) {

	
	}

	
	
	class Response
	{
		private CompletableFuture<Object> future;
		private int restquestId;
		public CompletableFuture<Object> getFuture() {
			return future;
		}
		public int getRestquestId() {
			return restquestId;
		}
		public void setFuture(CompletableFuture<Object> future) {
			this.future = future;
		}
		public void setRestquestId(int restquestId) {
			this.restquestId = restquestId;
		}
	}



	@Override
	public <T> void send(Object request, Consumer<T> backFunc, Consumer<Throwable> error) {
		
	}

	@Override
	public <T> void send(Object request, Consumer<T> backFunc, Consumer<Throwable> error, int timeOut) {
		
	}

}
