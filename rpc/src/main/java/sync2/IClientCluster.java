package sync2;

public interface IClientCluster 
{
	public IRpcClient banance();
	
	public IRpcClient next();
	
	public IRpcClient random();
	
	public void add(IRpcClient client);
	
	public void del(IRpcClient client);

}
