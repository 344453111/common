package sync2;

import java.util.concurrent.Executor;
import java.util.function.Consumer;

/**
 * rpc服务类
 * @author jinmiao
 * 2016年7月26日
 *
 */
public interface IRpcClient {
	
	/**
	 * 发送消息
	 * @param request
	 */
	public void send(Object request);
	
	/**
	 * 同步回调
	 * @param request
	 * @param timeOut
	 * @return
	 */
	public <T> T send(Object request,int timeOut);
	
	
	public <T> void send(Object request, Consumer<T> backFunc, Consumer<Throwable> error);
	
	/**
	 * 异步回调
	 * @param request
	 * @param succeed
	 * @param error
	 * @param timeOut
	 */
	public <T> void send(Object request,Consumer<T> backFunc,Consumer<Throwable> error,int timeOut);
	
	/**
	 * 异步回调
	 * @param request
	 * @param succeed
	 * @param error
	 */
	public <T> void send(Object request,Consumer<T> backFunc,Consumer<Throwable> error,Executor executor);
	
	/**
	 * 异步回调
	 * @param request
	 * @param succeed
	 * @param error
	 * @param timeOut
	 */
	public <T> void send(Object request,Consumer<T> backFunc,Consumer<Throwable> error,Executor executor,int timeOut);

}
