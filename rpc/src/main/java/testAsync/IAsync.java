package testAsync;

import java.util.concurrent.CompletableFuture;

/**
 * Created by JinMiao
 * 2017/4/24.
 */
public interface IAsync {

    CompletableFuture<String> getName(String name);

}
