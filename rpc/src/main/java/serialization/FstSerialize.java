package serialization;

import org.nustaq.serialization.FSTConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FstSerialize implements ISerialize {
	private static Logger log = LoggerFactory.getLogger(FstSerialize.class);

	private FSTConfiguration config = FSTConfiguration.getDefaultConfiguration();
	
	@Override
	public byte[] serialize(Object obj) {
		return config.asByteArray(obj);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T unSerialize(byte[] bytes, Class<T> t) {
		return (T)config.asObject(bytes);
	}

}
