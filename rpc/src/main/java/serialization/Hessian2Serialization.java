
package serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;

public class Hessian2Serialization implements ISerialize {

    @Override
    public byte[] serialize(Object data) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Hessian2Output out = new Hessian2Output(bos);
        try {
			out.writeObject(data);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return bos.toByteArray();
    }

	@SuppressWarnings("unchecked")
	@Override
	public <T> T unSerialize(byte[] byts, Class<T> t) {
        Hessian2Input input = new Hessian2Input(new ByteArrayInputStream(byts));
        try {
			return (T) input.readObject(t);
		} catch (IOException e) {
			e.printStackTrace();
		}
        return null;
    }
}
