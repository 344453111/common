package serialization;
/**
 * 序列化与反序列化接口
 * @author Administrator
 *
 */
public interface ISerialize {
	
	byte[] serialize(Object obj);
	
	<T> T unSerialize(byte[] byts, Class<T> t);
}
