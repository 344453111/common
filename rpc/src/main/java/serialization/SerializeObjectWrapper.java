package serialization;

import org.msgpack.annotation.Message;

import java.io.Serializable;
/**
 * 序列化对象包装
 * @author Jinmiao
 *
 */
@Message
public class SerializeObjectWrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Object object;
	
	
	public SerializeObjectWrapper(Object object) {
		super();
		this.object = object;
	}

	public SerializeObjectWrapper() {
		super();
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProtostuffWrapper [object=").append(object).append("]");
		return builder.toString();
	}
	
	
	
}
