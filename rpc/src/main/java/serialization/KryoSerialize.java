package serialization;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.FastOutput;
import com.esotericsoftware.kryo.io.Input;

public class KryoSerialize implements ISerialize{
	Kryo kryo = new Kryo();
	
	
//	private ByteBufferOutput output = new ByteBufferOutput(4096);
	
	private FastOutput output = new FastOutput(4096);
	public void KryoSerialize(){
		kryo.register(SerializeObjectWrapper.class);
		//kryo.setReferences(false);
		//kryo.setRegistrationRequired(true);
		//kryo.register(Arrays.asList().getClass(), new ArraysAsListSerializer());
	}



	@Override
	public byte[] serialize(Object obj) {
		SerializeObjectWrapper wrapper = new SerializeObjectWrapper(obj);
	    kryo.writeClassAndObject(output, wrapper);
	    byte[] b = output.toBytes();
	    output.clear();
		return b;
	}

	@Override
	public <T> T unSerialize(byte[] byts, Class<T> t) {
		Input input = new Input(byts);
		SerializeObjectWrapper wrapper = (SerializeObjectWrapper) kryo.readClassAndObject(input);
		return (T) wrapper.getObject();
	}
	
	
	
	
	

}
