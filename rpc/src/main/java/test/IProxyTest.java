package test;

import java.util.concurrent.CompletableFuture;

public interface IProxyTest {
	String aa(int a);
	
	void cc();
	
	CompletableFuture<String> asyncAA(int a);
	
}
