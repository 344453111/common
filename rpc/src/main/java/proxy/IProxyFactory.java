package proxy;

import java.lang.reflect.InvocationHandler;

public interface IProxyFactory {
	<T> T getProxy(Class<T> clz, InvocationHandler invocationHandler);
}
