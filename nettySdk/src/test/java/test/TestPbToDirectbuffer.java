package test;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.UnpooledByteBufAllocator;
import pb.TestPBMessages;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by JinMiao
 * 2019/11/1.
 */
public class TestPbToDirectbuffer {
    public static void main(String[] args) throws IOException {
        ByteBuf byteBuf = UnpooledByteBufAllocator.DEFAULT.directBuffer(10);
        ByteBuffer byteBuffer  = byteBuf.nioBuffer(0,byteBuf.capacity());

        //写入
        TestPBMessages.PingPongReq.Builder builder = TestPBMessages.PingPongReq.newBuilder();
        builder.setR(1);
        CodedOutputStream codedOutputStream = CodedOutputStream.newInstance(byteBuffer);
        builder.build().writeTo(codedOutputStream);
        codedOutputStream.flush();
        byteBuf.writerIndex(byteBuffer.position());

        //读取
        byteBuffer = byteBuf.nioBuffer();
        TestPBMessages.PingPongReq pingPongReq = TestPBMessages.PingPongReq.parseFrom(CodedInputStream.newInstance(byteBuffer));


        System.out.println(pingPongReq.getR());

        System.out.println();



    }
}
