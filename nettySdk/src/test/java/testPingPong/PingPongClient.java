package testPingPong;

import java.net.InetSocketAddress;

import common.Message;
import io.netty.channel.Channel;
import net.NetConnector;
import pb.TestPBMessages.PingPongReq;

public class PingPongClient {
	public static void main(String[] args) throws InterruptedException {
		TestEngin engin = new TestEngin();
		engin.inits();
		NetConnector conn = new NetConnector(engin);
		
		
		Channel channel = conn.connect(new InetSocketAddress("0.0.0.0", 45670));
		Message req = Message.newMessage();
		req.sethId(222);
		req.setmId(111);
		for(int i =0;i<100000;i++)
		{
//			Thread.sleep(1000);
			PingPongReq.Builder b  = PingPongReq.newBuilder();
			b.setR(i);
			req.setBody(b);
			req.setResponseId(5);
			channel.writeAndFlush(req);
		}
		
	}
}
