package testPingPong;

import common.AbsClientHandler;
import common.EnginService;
import common.Message;
import pb.TestPBMessages.PingPongRes;

public class ResHandler extends AbsClientHandler
{

	public ResHandler(EnginService service) {
		super(service);
	}

	@Override
	public void handler(Message msg) throws Throwable {
		PingPongRes req = msg.getBody();
//		System.out.println(msg.getResponseId());
//		System.out.println(req.getR());		
	}

	@Override
	public Object initBodyClass() {
		return PingPongRes.class;
	}

}
