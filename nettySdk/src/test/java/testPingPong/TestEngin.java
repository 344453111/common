package testPingPong;

import java.net.InetSocketAddress;

import code.HandlerManager;
import code.PBMessageManager;
import common.ChannelManager;
import common.EnginService;
import common.Message;
import io.netty.channel.Channel;
import net.NetAcceptor;
import net.NetConnector;
import pb.TestPBMessages.PingPongReq;
import pb.TestPBMessages.PingPongRes;

public class TestEngin extends EnginService{
	public void inits()
	{
		this.messageManager = new PBMessageManager();
		this.handlerManager = new HandlerManager();
		this.channelManager = new ChannelManager();
		
		this.handlerManager.addHandler(222, 111, new ResHandler(this));
		this.handlerManager.addHandler(111, 222, new ReqHandler(this));
		this.messageManager.addMessageCla(222, 111, PingPongRes.class);
		this.messageManager.addMessageCla(111, 222, PingPongReq.class);
		
		this.init();
	}
	
	
	
	
	
	
	
}
