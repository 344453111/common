package testPingPong;

import common.AbsClientHandler;
import common.EnginService;
import common.Message;
import pb.TestPBMessages.PingPongReq;
import pb.TestPBMessages.PingPongRes;

public class ReqHandler extends AbsClientHandler
{

	public ReqHandler(EnginService service) {
		super(service);
	}

	@Override
	public void handler(Message msg) throws Throwable {
		PingPongReq req = msg.getBody();
//		System.out.println(msg.getResponseId());
//		System.out.println(req.getR());
		PingPongRes.Builder b = PingPongRes.newBuilder();
		b.setR(req.getR()+1);
		
		msg.sethId(111);
		msg.setmId(222);
		msg.setBody(b);
		msg.setResponseId(6);
		msg.getChannel().write(msg);
		
	}

	@Override
	public Object initBodyClass() {
		return PingPongReq.class;
	}

}
