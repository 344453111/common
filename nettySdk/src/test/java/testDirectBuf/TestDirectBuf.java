package testDirectBuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

public class TestDirectBuf {
	public static void main(String[] args) {
		
		
		
		PooledByteBufAllocator allocator = new PooledByteBufAllocator(true);
		
		allocator.buffer();
		ByteBuf bb1 = allocator.buffer();
		ByteBuf bb2 = allocator.buffer();
		String str = "哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈";
		byte[] bytes = str.getBytes();
		bb1.writeBytes(bytes);
		
		for(int i=0;i<100000;i++)
		{
			normal(bb1);
			directBuf(bb1);
		}
		
		
		long start = System.currentTimeMillis();
		for(int i=0;i<1000000;i++)
		{
			normal(bb1);
//			directBuf(bb1, bb2);
		}
		System.out.println(System.currentTimeMillis() - start);
		
		start = System.currentTimeMillis();
		for(int i=0;i<1000000;i++)
		{
//			normal(bb1);
			directBuf(bb1);
		}
		System.out.println(System.currentTimeMillis() - start);
	}
	
	
	
	private static void normal(ByteBuf bb1)
	{
		byte[] bytes =new byte[507];
		bb1.readBytes(bytes);
		bb1.clear();
		bb1.writeBytes(bytes);
	}
	
	private static void directBuf(ByteBuf bb1)
	{
//		buffer.slice(index, length).retain()
//		System.out.println(bb1.readerIndex());
		ByteBuf b2 = bb1.slice(0, 507).retain();
//		bb1.readerIndex(bb1.readerIndex()+507);
//		System.out.println(bb1.readerIndex());
//		System.out.println(b2.hashCode());
//		System.out.println(bb1.hashCode());
//		System.out.println();
//		bb2.writeBytes(bb1);
//		bb1.writeBytes(b2, 0, 507);
//		bb1.writeBytes(b2);
//		bb1.release(1);
//		b2.release();
	}
}
