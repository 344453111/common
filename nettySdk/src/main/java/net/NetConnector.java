package net;

import java.net.InetSocketAddress;

import org.apache.log4j.Logger;

import code.ClientChannelInitializer;
import common.EnginService;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * 网络连接器
 * 
 * @author King
 * 
 */
public class NetConnector
{
	private static final Logger log = Logger.getLogger(NetConnector.class);
	private EventLoopGroup group;
	private Bootstrap b;
	private Channel channel;
	
	
	public NetConnector(EnginService engin) 
	{
		// netty connector初始化
		ClientChannelInitializer handler = new ClientChannelInitializer();
		handler.setEnginService(engin);
		this.group = new NioEventLoopGroup();
		this.b = new Bootstrap();
		this.b.group(group).channel(NioSocketChannel.class).handler(handler);
		b.option(ChannelOption.TCP_NODELAY, true);
		b.option(ChannelOption.SO_SNDBUF, 2048);
		b.option(ChannelOption.SO_RCVBUF, 8096);
		b.option(ChannelOption.SO_KEEPALIVE, true);
		b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
	}
	
	
	public Channel connect(InetSocketAddress... address) throws InterruptedException
	{
		for(InetSocketAddress addr:address)
		{
			ChannelFuture future = b.connect(addr);
			future.sync();
			log.warn("connect to " + address+" success");
			return future.channel();
		}
		return null;
	}
	

	/**
	 * 关闭
	 */
	public void shutdown() {
		if(this.channel!=null)
			this.channel.close();
		if(this.group!=null)
			this.group.shutdownGracefully();
	}

	public Channel getChannel() {
		return channel;
	}
}
