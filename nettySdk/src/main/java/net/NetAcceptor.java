package net;

import java.net.InetSocketAddress;

import javax.management.RuntimeErrorException;

import code.AbsChannelInitializer;
import code.ServerChannelInitializer;
import common.EnginService;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 网络监听
 * @author King
 *
 */
public class NetAcceptor
{
	private EventLoopGroup bossGroup;
	private EventLoopGroup workerGroup;
	private ServerBootstrap b;
	private Channel channel;
	
	public NetAcceptor(EnginService engin,InetSocketAddress address)
	{
		AbsChannelInitializer handler = new ServerChannelInitializer();
		handler.setEnginService(engin);
		// handler
		this.bossGroup = new NioEventLoopGroup();
		this.workerGroup = new NioEventLoopGroup();
		this.b = new ServerBootstrap();
		b.option(ChannelOption.SO_BACKLOG, 128);
		b.childOption(ChannelOption.TCP_NODELAY, true);
		b.childOption(ChannelOption.SO_SNDBUF, 2048);
		b.childOption(ChannelOption.SO_RCVBUF, 8096);
		b.childOption(ChannelOption.SO_KEEPALIVE, true);
		b.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
		
//		b.option(option, value)
		
		
//		cfg.setReuseAddress(true);
//		cfg.setTcpNoDelay(true);
//		cfg.setKeepAlive(true);
//		cfg.setSoLinger(0);
		
		b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
				.childHandler(handler);
		try {
			this.channel = b.bind(address).sync().channel();
		} catch (Exception e) {
			 this.workerGroup.shutdownGracefully();
	         this.bossGroup.shutdownGracefully();
	         throw new RuntimeException("error",e);
		}
	}
	
	/**
	 * 关闭服务器用
	 */
	public void shutdown()
	{
		this.channel.close();
		this.workerGroup.shutdownGracefully();
        this.bossGroup.shutdownGracefully();
	}

	public Channel getChannel() {
		return channel;
	}

}
