package common;

public abstract class AbsClientHandler extends AbsThreadHandler {
	
	protected EnginService service;
	
	public AbsClientHandler(EnginService service)
	{
		this.service = service;
	}
}
