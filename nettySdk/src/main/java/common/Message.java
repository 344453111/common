package common;

import com.google.protobuf.GeneratedMessage;

/**
 * 消息对象
 * @author pc
 *
 */
public class Message{
	/**模块**/
	public static final int M_ID = -1;
	/**通知业务层断开连接了   在业务层注册该消息**/
	public static final int H_DISCONNET = 1;
	/**发送心跳**/
	public static final int H_SEND_HEART_BEAT = 2;
	/**接收心跳返回**/
	public static final int H_RECIEVE_HEART_BEAT= 2000;

	
	/**消息最大 {@link Integer#MAX_VALUE}    消息头长度 4字节**/
	public static final int MAX_MESSAGE_SIZE = Integer.MAX_VALUE,HEAD_SIZE = 4;
	/**模块id**/
	private int mId;
	/**业务id**/
	private int hId;
	/**回调id**/
	private int responseId;
	/**消息体**/
	private Object obj;
	
	private NetChannel channel;
	
	
	/**
	 * 在写业务的时候不要使用该方法方便扩展
	 * 替代方案{@link Message#newMessage()}
	 */
	protected Message()
	{
		
	}
	
	/**获取一个新message**/
	public static Message newMessage()
	{
//		MyMessage msg = threadLocal.get();
//		if(msg==null)
//		{
//			msg = new MyMessage();
//			threadLocal.set(msg);
//		}else
//			msg.clearDate();
		return new Message();
//		return new MyMessage();
//		if(debug)
//		{
//			return new MyMessage();
//		}
//		message.clearData();
//		return message;
	}	
	
	@SuppressWarnings("unchecked")
	public <T> T getBody()
	{
		return (T)obj;
	}
	
	public void setBody(Object obj)
	{
		this.obj = obj;
	}

	
	@SuppressWarnings("rawtypes")
	public void setBody(GeneratedMessage.Builder builder)
	{
		this.obj = builder.build();
	}
	

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int gethId() {
		return hId;
	}

	public void sethId(int hId) {
		this.hId = hId;
	}

	public int getResponseId() {
		return responseId;
	}

	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}

	public NetChannel getChannel() {
		return channel;
	}

	public void setChannel(NetChannel channel) {
		this.channel = channel;
	}
}
