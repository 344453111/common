package common;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import threadPool.task.OutTask;

/**
 * http
 * @author King
 *
 */
public abstract class HttpHandler extends OutTask
{

	private AbstractHttpHandler handler;
	private HttpContent msg;
	//private HttpContent msgContent;
	private ChannelHandlerContext ctx;
	private HttpMethod method;
	private HttpRequest request;
	
	public HttpHandler(AbstractHttpHandler handler,Object msg, ChannelHandlerContext ctx, HttpMethod method, HttpRequest request)
	{
		this.handler = handler;
		
		//DefaultLastHttpContent lastHttpContent = (DefaultLastHttpContent)msg;
		this.msg = ((HttpContent)msg).copy();
		
		this.ctx = ctx;
		this.method = method;
		this.request = request;
	}
	
	
	
	public void execute() throws Throwable
	{
		handler.process(msg, ctx,method,request);
	}


	public String getExecuteName() {
		return handler.getExecuteName();
	}

}
