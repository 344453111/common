package common;

import io.netty.util.AttributeKey;

public class ServerAttributeKey
{

	public static final AttributeKey<Long> channel_Id =  AttributeKey.valueOf("channelId");
	
	public static final AttributeKey<NetChannel> netChannel = AttributeKey.valueOf("netChannel");
	
	
}
