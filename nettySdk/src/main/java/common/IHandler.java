package common;

public interface IHandler{
	void handler(Message msg) throws Throwable;
	
	/**
	 * 初始化消息体对象
	 * @return
	 */
	Object initBodyClass();
	
	/**获得线程名字**/
	String getExecuteName();
	
}
