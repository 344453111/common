package common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import threadPool.task.OutTask;

public class BaseTask extends OutTask
{
	private static final Logger log = LoggerFactory.getLogger(BaseTask.class); 
	private Message msg;
	
	private IHandler handler;
	
	public BaseTask(Message msg,IHandler handler,EnginService service)
	{
		this.msg = msg;
		this.handler = handler;
	}
	
	
	

	public String getExecuteName() {
		if(handler==null)
			return null;
		return handler.getExecuteName();
	}


	@Override
	public void execute() throws Throwable {
		if(handler==null)
		{
			log.error("no exist handler，mid:"+msg.getmId()+"  "+msg.gethId());
			return;
		}
		handler.handler(msg);
	}
}
