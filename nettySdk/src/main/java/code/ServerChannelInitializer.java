package code;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * 服务器用的
 * @author King
 *
 */
public class ServerChannelInitializer extends AbsChannelInitializer<SocketChannel>
{
	@Override
	protected void initChannel(SocketChannel ch) throws Exception
	{
		ChannelPipeline pipeline = ch.pipeline();
		//tcp丢包断包处理  消息头增加4字节标记消息长度
		pipeline.addLast("frameDecoder",new MyLengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 0));
		pipeline.addLast("frameEncoder",new MyLengthFieldPrepender());
		PBBytesToMessageDecode decode = new PBBytesToMessageDecode();
		decode.setMessageManager(this.service.getMessageManager());
		pipeline.addLast("byteToMyMessageDecoder", decode);
		//encode
		pipeline.addLast("myMessageToByteEncoder",new PBMssageToBytesEncode());
		//心跳
		pipeline.addLast(new IdleStateHandler(30, 0, 0));
		ServerChannelHandler handler = new ServerChannelHandler();
		handler.setEnginService(this.service);
		pipeline.addLast("liangShanHandler",handler);
	}
}
