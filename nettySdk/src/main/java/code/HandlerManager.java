package code;

import java.util.HashMap;
import java.util.Map;

import common.IHandler;



/**
 * 业务处理管理类
 * @author jinmiao
 *
 */
public class HandlerManager implements IHandlerManager
{

	private final Map<Integer, Map<Integer, IHandler>> handlerMap = new HashMap<Integer, Map<Integer, IHandler>>();

	
	/**
	 * 增加一个业务处理类
	 * @param modelId
	 * @param actionId
	 * @param handler
	 */
	public void addHandler(int mid, int hid,IHandler handler)
	{
		Map<Integer, IHandler> handlers = handlerMap.get(mid);
		if(handlers==null)
			handlers = new HashMap<Integer, IHandler>();
		handlers.put(hid, handler);
		handlerMap.put(mid, handlers);
	}
	
	/**
	 * 获取一个业务处理类
	 * @param modelId
	 * @param actionId
	 * @return
	 */
	public IHandler getHandler(int mid, int hid)
	{
		final Map<Integer, IHandler> handlers = handlerMap.get(mid);
		if(handlers==null)
		{
//			logger.error("modelId:"+modelId+"actionId:"+actionId+" not found");
			return null;
		}
		return handlers.get(hid);
	}
}
