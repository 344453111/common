package code;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.BaseTask;
import common.EnginService;
import common.IHandler;
import common.Message;
import common.NetChannel;
import common.ServerAttributeKey;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.Attribute;

/**
 * 服务器网络层业务接受handler
 * 由此像业务层派发业务
 * @author King
 *
 */
public class ServerChannelHandler extends SimpleChannelInboundHandler<Message> 
{
	
	private static final Logger log = LoggerFactory.getLogger(ServerChannelHandler.class); 
	protected EnginService service;
	
	public void setEnginService(EnginService service)
	{
		this.service = service;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);
		service.getChannelManager().addChannel(ctx.channel());
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Message msg)
			throws Exception
	{
		IHandler handler = service.getHandlerManager().getHandler(msg.getmId(), msg.gethId());
		BaseTask task = new BaseTask(msg,handler,service);
		try {
			if(service.getThreadService()==null)
			{
				task.execute();
				return;
			}
			service.getThreadService().publishTask(task);
		} catch (Throwable e) {
			log.error("error",e);
//			throw new Exception(e);
		}
	}
	
	
	@Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (!(evt instanceof IdleStateEvent)) {
            return;
        }
        IdleStateEvent e = (IdleStateEvent) evt;
        //2次read空闲超时就断开连接
        if (e.state() == IdleState.READER_IDLE) {
        	Attribute<NetChannel> attr = ctx.channel().attr(ServerAttributeKey.netChannel);
        	NetChannel channel = attr.get();
        	if(channel!=null)
        	{
        		if(!channel.isHeartBeat())
        		{
        			channel.setHeartBeat(true);
        			return;
        		}
        	}
        	ctx.close();
//        	losetConnect(ctx);
        }
    }
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception
	{
		super.channelInactive(ctx);
		losetConnect(ctx);
	}
	
	/**
	 * 丢失连接   
	 * 模拟玩家退出游戏消息
	 * @param ctx
	 */
	private void losetConnect(ChannelHandlerContext ctx) throws Exception
	{
		Attribute<NetChannel> attr = ctx.channel().attr(ServerAttributeKey.netChannel);
		NetChannel channel = attr.get();
		service.getChannelManager().delChannel(ctx.channel());
		if(channel==null||channel.isKick())
		{
			return;
		}
		//抛出掉线消息给业务线程
		Message msg = Message.newMessage();
		msg.setChannel(attr.get());
		msg.setmId(Message.M_ID);
		msg.sethId(Message.H_DISCONNET);
		IHandler handler = service.getHandlerManager().getHandler(msg.getmId(), msg.gethId());
		BaseTask task = new BaseTask(msg,handler,service);
		try {
			if(service.getThreadService()==null)
			{
				task.execute();
				return;
			}
			service.getThreadService().publishTask(task);
		} catch (Throwable e) {
			log.error("error",e);
		}
	}
	
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		super.exceptionCaught(ctx, cause);
		ctx.close();
	}
	
}
