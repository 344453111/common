package code;

import common.EnginService;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;

/**
 * channel filter抽象类
 * @author King
 *
 */
public abstract class AbsChannelInitializer<T extends  Channel> extends ChannelInitializer<T>
{
	
	protected EnginService service;
	
	public void setEnginService(EnginService service)
	{
		this.service = service;
	}
}
