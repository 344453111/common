package code;

import com.google.protobuf.MessageLite;

import common.Message;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.MessageToByteEncoder;
import utils.ByteUtils;

/**
 * 客户端编码
 * 将mymessage转成二进制
 * @author King
 *
 */
public class PBMssageToBytesEncode extends MessageToByteEncoder<Message>
{
	
	@Override
	public void close(ChannelHandlerContext ctx, ChannelPromise promise)
			throws Exception {
		super.close(ctx, promise);
	}

	
	
	@Override
	protected void encode(ChannelHandlerContext ctx, Message msg, ByteBuf out)
			throws Exception {
		ByteUtils.writeVariaInt(msg.getmId(), out);
		ByteUtils.writeVariaInt(msg.gethId(), out);
		ByteUtils.writeVariaInt(msg.getResponseId(), out);
		byte[] bytes =null;
		if(msg.getBody()!=null)
		{
			MessageLite body = msg.getBody();
			bytes = body.toByteArray();
		}
		if(bytes!=null)
		{
			out.writeBytes(bytes);
			bytes= null;
		}
		msg=null;
		
	}
}
