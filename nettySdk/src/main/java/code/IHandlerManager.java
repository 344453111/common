package code;

import common.IHandler;

/**
 * 业务处理类管理类  
 * @author King
 *
 */
public interface IHandlerManager
{

	/**
	 * 增加一个业务处理类
	 * @param modelId
	 * @param actionId
	 * @param handler
	 */
	public abstract void addHandler(int modelId, int actionId, IHandler handler);

	/**
	 * 获取一个业务处理类
	 * @param modelId
	 * @param actionId
	 * @return
	 */
	public abstract IHandler getHandler(int modelId, int actionId);

}