package rpc.sync;

public interface IRpcService {
	/**
	 * 调用rpc服务
	 * @param cls
	 * @return
	 */
	public <T> T rpcService(Class<T> cls);
}
