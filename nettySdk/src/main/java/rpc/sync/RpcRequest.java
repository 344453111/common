package rpc.sync;

public class RpcRequest
{
	private String interfaceName;
	
    private String methodName;
//    private String paramtersDesc;
    private Object[] arguments;
    /**请求id**/
    private long requestId;
    /**协议版本号**/
    private int protocolVersion;
    
	public String getInterfaceName() {
		return interfaceName;
	}
	public String getMethodName() {
		return methodName;
	}
	public Object[] getArguments() {
		return arguments;
	}
	public long getRequestId() {
		return requestId;
	}
	public int getProtocolVersion() {
		return protocolVersion;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public void setArguments(Object[] arguments) {
		this.arguments = arguments;
	}
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
}
