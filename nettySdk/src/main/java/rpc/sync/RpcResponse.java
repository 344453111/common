package rpc.sync;

public class RpcResponse {
	/**回调id**/
	private int responseId;
	/**回调消息体**/
	private Object obj;
	
	
	public int getResponseId() {
		return responseId;
	}
	public Object getObj() {
		return obj;
	}
	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
}
