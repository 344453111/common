package rpc.sync;

import java.util.concurrent.ConcurrentHashMap;
/**
 * 回调管理类
 * @author pc
 *
 */
public class ResponseManager
{
	private ConcurrentHashMap<Integer, Request> responseMap = new ConcurrentHashMap<>();

	public ConcurrentHashMap<Integer, Request> getResponseMap() {
		return responseMap;
	}

	public void setResponseMap(ConcurrentHashMap<Integer, Request> responseMap) {
		this.responseMap = responseMap;
	}
}
