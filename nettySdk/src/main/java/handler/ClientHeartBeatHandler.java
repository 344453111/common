package handler;

import common.AbsThreadHandler;
import common.Message;
import common.NetChannel;

/**
 * 客户端心跳包
 * @author pc
 *
 */
public class ClientHeartBeatHandler extends AbsThreadHandler
{

	public void handler(Message msg) throws Throwable
	{
		NetChannel channel = msg.getChannel();
		channel.setHeartBeat(false);
	}

	public Class<Object> initBodyClass() {
		return null;
	}

}
