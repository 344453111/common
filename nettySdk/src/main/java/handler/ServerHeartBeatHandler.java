package handler;

import common.AbsThreadHandler;
import common.Message;
import common.NetChannel;
/**
 * 服务器收到心跳
 * @author pc
 *
 */
public class ServerHeartBeatHandler extends AbsThreadHandler {

	public void handler(Message msg) throws Throwable
	{
		NetChannel channel = msg.getChannel();
		channel.setHeartBeat(false);
		//发送心跳返回包
		Message nMsg = Message.newMessage();
		nMsg.setmId(Message.M_ID);
		nMsg.sethId(Message.H_RECIEVE_HEART_BEAT);
		channel.write(nMsg);
	}

	public Class<Object> initBodyClass() {
		return null;
	}
}
