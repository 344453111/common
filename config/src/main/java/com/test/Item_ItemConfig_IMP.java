package com.test;

import java.util.Comparator;

import com.bean.GameModel;
import com.utils.KV;

public class Item_ItemConfig_IMP extends Item_ItemConfig {

	
	public static final String INDEX_TYPE_FUNCID="type##funcId";
	@Override
	public void init() {
		System.out.println("初始化"+id);
	}
	
	@Override
	public String[] getComplexIndex() {
		return new String[]{INDEX_TYPE_FUNCID,PRIMARY_KEY};
	}

	
	@Override
	public boolean isHash() {
		return true;
	}
	
	
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	@Override
//	public KV<String, Comparator<GameModel>>[] getSort() {
//		return new KV[]{new KV(PRIMARY_KEY,new Comparator<Item_ItemConfig_IMP>(){
//			public int compare(Item_ItemConfig_IMP o1, Item_ItemConfig_IMP o2) {
//				if(o1.getId()>o2.getId())
//					return -1;
//				return 0;
//			}
//			
//		})};
//	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public KV<String, Comparator<GameModel>>[] getSort() {
		Comparator<Item_ItemConfig_IMP> c = (o1, o2) -> {
			if (o1.getId() > o2.getId())
				return -1;
			return 0;
		};
		return new KV[] { new KV(PRIMARY_KEY, c) };
	}
	
}
