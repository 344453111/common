package com.test;

import com.bean.GameModel;

/**
  * 类描述
  *	自动生成的 
  *
**/  
public abstract class XLS_ITEM_ITEM extends GameModel 
{
 
 	/**id描述**/
	public int id;
 	/**name描述**/
	public String name;

	public int getId()
	{
		return this.id;
	}
	public String getName()
	{
		return this.name;
	}

	public static final String fileName = "XLS_ITEM_ITEM";


@Override
public String toString() {
	return "XLS_ITEM_ITEM["+
		"id="+this.id+","+
		"name="+this.name+","+
	"]";
	}
}

