package com.test;


import com.bean.GameModel;
/**
 * Item.xls
 * 自动生成的,请勿修改
 *
 */
public abstract class Item_EquipConfig extends GameModel 
{
 
 	/**id**/
	public int id;
 	/**装备位**/
	public String position;
 	/**属性类型1**/
	public String proType1;
 	/**属性值1**/
	public int proval1;
 	/**属性类型2**/
	public String proType2;
 	/**属性值2**/
	public int proval2;
 	/**属性类型3**/
	public String proType3;
 	/**属性值3**/
	public int proval3;
 	/**属性类型4**/
	public String proType4;
 	/**属性值4**/
	public int proval4;
 	/**属性类型5**/
	public String proType5;
 	/**属性值5**/
	public int proval5;
 	/**属性类型6**/
	public String proType6;
 	/**属性值6**/
	public int proval6;
 	/**属性类型7**/
	public String proType7;
 	/**属性值7**/
	public int proval7;

	public int getId()
	{
		return this.id;
	}
	public String getPosition()
	{
		return this.position;
	}
	public String getProType1()
	{
		return this.proType1;
	}
	public int getProval1()
	{
		return this.proval1;
	}
	public String getProType2()
	{
		return this.proType2;
	}
	public int getProval2()
	{
		return this.proval2;
	}
	public String getProType3()
	{
		return this.proType3;
	}
	public int getProval3()
	{
		return this.proval3;
	}
	public String getProType4()
	{
		return this.proType4;
	}
	public int getProval4()
	{
		return this.proval4;
	}
	public String getProType5()
	{
		return this.proType5;
	}
	public int getProval5()
	{
		return this.proval5;
	}
	public String getProType6()
	{
		return this.proType6;
	}
	public int getProval6()
	{
		return this.proval6;
	}
	public String getProType7()
	{
		return this.proType7;
	}
	public int getProval7()
	{
		return this.proval7;
	}

public String getFileName()
{
	return "Item.xls_Equip";
}


@Override
public String toString() {
	return "Item_EquipConfig["+
		"id="+this.id+","+
		"position="+this.position+","+
		"proType1="+this.proType1+","+
		"proval1="+this.proval1+","+
		"proType2="+this.proType2+","+
		"proval2="+this.proval2+","+
		"proType3="+this.proType3+","+
		"proval3="+this.proval3+","+
		"proType4="+this.proType4+","+
		"proval4="+this.proval4+","+
		"proType5="+this.proType5+","+
		"proval5="+this.proval5+","+
		"proType6="+this.proType6+","+
		"proval6="+this.proval6+","+
		"proType7="+this.proType7+","+
		"proval7="+this.proval7+","+
	"]";
	}
}
