package com.test;

import java.util.List;
import java.util.Set;

import com.ConfigManager;
import com.bean.GameModel;

public class TestManager {
	public static void main(String[] args) throws InstantiationException, IllegalAccessException {
		ConfigManager manager = new ConfigManager();
		manager.setResourceFolder("C:/Users/qipeng/Desktop");
		manager.addModel(Item_ItemConfig_IMP.class);
		manager.init();
		
		//查找id 字段为3的一行
		Item_ItemConfig_IMP config = manager.getById(Item_ItemConfig_IMP.class, 3);
		System.out.println(config.toString());
		
		//查找type为3 funcId为0 的一行
		config = manager.getTemplateByIndex(Item_ItemConfig_IMP.class, Item_ItemConfig_IMP.INDEX_TYPE_FUNCID, 3,0);
		System.out.println(config.toString());
		
		//查找type为1 funcId为1 的多行 set集合
		Set<Item_ItemConfig_IMP> set = manager.getTemplateSetByIndex(Item_ItemConfig_IMP.class, Item_ItemConfig_IMP.INDEX_TYPE_FUNCID, 1,1);
		System.out.println(set.size());
		
		//获得根据id 排序好的集合
		List<Item_ItemConfig_IMP> list = manager.getTemplateSortListByIndex(Item_ItemConfig_IMP.class, GameModel.PRIMARY_KEY);
		System.out.println(list.size());
		
		//查找type为1 funcId为1 的多行  list集合
		list = manager.getTemplateListByIndex(Item_ItemConfig_IMP.class, Item_ItemConfig_IMP.INDEX_TYPE_FUNCID, 1,1);
		
		System.out.println();
		
		
	}
}
