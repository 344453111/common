package com.test;


import com.bean.GameModel;
/**
 * Item.xls
 * 自动生成的,请勿修改
 *
 */
public abstract class Item_ItemConfig extends GameModel 
{
 
 	/**id**/
	public int id;
 	/**名字多语言**/
	public String str_name;
 	/**描述多语言**/
	public String str_desc;
 	/**效果描述**/
	public String effect;
 	/**描述多语言**/
	public String str_effect;
 	/**物品大类**/
	public int type;
 	/**功能id**/
	public int funcId;
 	/**功能参数**/
	public String funcParam;
 	/**稀有度**/
	public int quality;
 	/**绑定模式**/
	public int bind;
 	/**等级限制**/
	public int levelLimit;
 	/**所属阵营**/
	public int camp;
 	/**所属职业**/
	public int occupation;
 	/**使用是否弹出提示**/
	public int prompt3;
 	/**是否可出售**/
	public int sell;
 	/**贩卖是否提示**/
	public int prompt1;
 	/**出售所得**/
	public int sellNum;
 	/**是否可销毁**/
	public int destroy;
 	/**销毁是否提示**/
	public int prompt;
 	/**购买是否提示**/
	public int prompt2;
 	/**是否可交易**/
	public int trading;
 	/**叠加数量限制**/
	public int overNum;
 	/**掉落公告**/
	public String notice;
 	/**使用CD（毫秒）**/
	public int cd;
 	/**公共CD（毫秒）**/
	public int publiccd;
 	/**物品消失时间**/
	public int disappeartime;
 	/**物品存在时间（秒）**/
	public int theretime;
 	/**使用物品后的行为**/
	public int usetype;
 	/**读条时间（毫秒）**/
	public int time;
 	/**可使用区域**/
	public String area;
 	/**能否放入仓库**/
	public int storage;
 	/**是否直接批量使用**/
	public int batch;
 	/**图标**/
	public String icon;

	public int getId()
	{
		return this.id;
	}
	public String getStr_name()
	{
		return this.str_name;
	}
	public String getStr_desc()
	{
		return this.str_desc;
	}
	public String getEffect()
	{
		return this.effect;
	}
	public String getStr_effect()
	{
		return this.str_effect;
	}
	public int getType()
	{
		return this.type;
	}
	public int getFuncId()
	{
		return this.funcId;
	}
	public String getFuncParam()
	{
		return this.funcParam;
	}
	public int getQuality()
	{
		return this.quality;
	}
	public int getBind()
	{
		return this.bind;
	}
	public int getLevelLimit()
	{
		return this.levelLimit;
	}
	public int getCamp()
	{
		return this.camp;
	}
	public int getOccupation()
	{
		return this.occupation;
	}
	public int getPrompt3()
	{
		return this.prompt3;
	}
	public int getSell()
	{
		return this.sell;
	}
	public int getPrompt1()
	{
		return this.prompt1;
	}
	public int getSellNum()
	{
		return this.sellNum;
	}
	public int getDestroy()
	{
		return this.destroy;
	}
	public int getPrompt()
	{
		return this.prompt;
	}
	public int getPrompt2()
	{
		return this.prompt2;
	}
	public int getTrading()
	{
		return this.trading;
	}
	public int getOverNum()
	{
		return this.overNum;
	}
	public String getNotice()
	{
		return this.notice;
	}
	public int getCd()
	{
		return this.cd;
	}
	public int getPubliccd()
	{
		return this.publiccd;
	}
	public int getDisappeartime()
	{
		return this.disappeartime;
	}
	public int getTheretime()
	{
		return this.theretime;
	}
	public int getUsetype()
	{
		return this.usetype;
	}
	public int getTime()
	{
		return this.time;
	}
	public String getArea()
	{
		return this.area;
	}
	public int getStorage()
	{
		return this.storage;
	}
	public int getBatch()
	{
		return this.batch;
	}
	public String getIcon()
	{
		return this.icon;
	}

public String getFileName()
{
	return "Item.xls_Item";
}


@Override
public String toString() {
	return "Item_ItemConfig["+
		"id="+this.id+","+
		"str_name="+this.str_name+","+
		"str_desc="+this.str_desc+","+
		"effect="+this.effect+","+
		"str_effect="+this.str_effect+","+
		"type="+this.type+","+
		"funcId="+this.funcId+","+
		"funcParam="+this.funcParam+","+
		"quality="+this.quality+","+
		"bind="+this.bind+","+
		"levelLimit="+this.levelLimit+","+
		"camp="+this.camp+","+
		"occupation="+this.occupation+","+
		"prompt3="+this.prompt3+","+
		"sell="+this.sell+","+
		"prompt1="+this.prompt1+","+
		"sellNum="+this.sellNum+","+
		"destroy="+this.destroy+","+
		"prompt="+this.prompt+","+
		"prompt2="+this.prompt2+","+
		"trading="+this.trading+","+
		"overNum="+this.overNum+","+
		"notice="+this.notice+","+
		"cd="+this.cd+","+
		"publiccd="+this.publiccd+","+
		"disappeartime="+this.disappeartime+","+
		"theretime="+this.theretime+","+
		"usetype="+this.usetype+","+
		"time="+this.time+","+
		"area="+this.area+","+
		"storage="+this.storage+","+
		"batch="+this.batch+","+
		"icon="+this.icon+","+
	"]";
	}
}
