package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.StringTemplateResourceLoader;

import com.bean.ClassBean;
import com.bean.FieldsBean;

public class TestBeetl {
	public static void main(String[] args) throws IOException {
		String res = readTxtFile("G:/workspace/config/src/main/resources/javaModel.txt");

		StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
		Configuration cfg = Configuration.defaultConfiguration();
		GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
		Template t = gt.getTemplate(res);
		
		ClassBean clsBean = new ClassBean();
		clsBean.desc = "类描述";
		clsBean.name = "XLS_ITEM_ITEM";
		clsBean.fatherName = "GameModel";
		clsBean.fileName = "XLS_ITEM_ITEM";
		
		FieldsBean intBean = new FieldsBean();
		intBean.name = "id";
		intBean.type = "int";
		intBean.desc =  "id描述";
		intBean.bigName = "Id";
		clsBean.fieldList.add(intBean);
		
		intBean = new FieldsBean();
		intBean.name = "name";
		intBean.type = "String";
		intBean.desc =  "name描述";
		intBean.bigName = "Name";
		clsBean.fieldList.add(intBean);
		t.binding("class",clsBean);
		
		String str = t.render();
		System.out.println(str);
		
	}
	
	
	public static String readTxtFile(String filePath) {
		StringBuffer sb = new StringBuffer();
		try {
			String encoding = "UTF-8";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					sb.append(lineTxt + "\n");
					// System.out.println(lineTxt);
				}
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
		}
		return sb.toString();
	}
	
}
