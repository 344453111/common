package com.utils;

/**
 * kv键值对
 * @author admin
 *
 * @param <K>
 * @param <V>
 */
public class KV<K,V> {
	private K k;
	private V v;
	
	private KV()
	{
		
	}
	
	public KV(K k ,V v){
		this.k = k;
		this.v = v;
	}
	
	
	public KV(String primaryKey, Object object) {
		// TODO Auto-generated constructor stub
	}


	@SuppressWarnings("rawtypes")
	public static KV createKv()
	{
		return new KV();
	}
	

	public K getK() {
		return k;
	}





	public void setK(K k) {
		this.k = k;
	}





	public V getV() {
		return v;
	}





	public void setV(V v) {
		this.v = v;
	}
	
	
	
}
