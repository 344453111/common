package com.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class FileUtils {
	
	final static Logger logger = Logger.getLogger(FileUtils.class);
	
	
	public static Workbook readExcel(String filePath) {
		Workbook wb = null;
		File file = new File(filePath);
		try {
			wb = WorkbookFactory.create(file);
//			if (filePath.endsWith(".xls")) {
//				wb = (Workbook) new HSSFWorkbook(new POIFSFileSystem(
//						new FileInputStream(file)));
//			} else if (filePath.endsWith(".xlsx")) {
//				wb = (Workbook) new XSSFWorkbook(new FileInputStream(file));
//			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return wb;
	}
	
	public static Map<String, String> readMap(String fileName) {
		Map<String, String> map = new HashMap<String, String>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "UTF-8"));
			String line = null;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.startsWith("#")) {
					continue;
				}
				String[] temp = line.split("=", 2);
				if (temp.length == 2) {
					map.put(temp[0].trim(), temp[1].trim());
				}
			}
		} catch (Exception e) {
			logger.error("read {} fail ", e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (Exception e) {
			}
		}
		return map;
	}
	
	
	/**
	 * 将内容写入文件里
	 * 
	 * @param fileContent
	 * @param fileName
	 * @param encoding
	 * @return add access or fail
	 */
	public static boolean writeStringFile(String fileContent, String fileName,
			String encoding)
	{
		try
		{
			MakeDirs(getFileNamePath(fileName));
			File file = new File(fileName);
			if(file.exists())
			{
				file.delete();
			}
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			byte[] b = fileContent.getBytes(encoding);
			fileOutputStream.write(b);
			fileOutputStream.flush();
			fileOutputStream.close();
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	

	/**
	 * 创建文件夹
	 * 
	 * @param path
	 * @return realPath
	 */
	public static String MakeDirs(String path)
	{
		File f = new File(path);
		if (!f.exists())
		{
			f.mkdirs();
		}
		return f.getPath();
	}
	/**
	 * 得到文件路径
	 * 
	 * @param fileName
	 * @return file real path
	 */
	public static String getFileNamePath(String fileName)
	{
		int pos = fileName.lastIndexOf("\\");
		int pos2 = fileName.lastIndexOf("/");
		if (pos == -1 && pos2 == -1)
		{
			return "";
		}
		else
		{
			if (pos2 > pos)
			{
				return fileName.substring(0, pos2);
			}
			else
			{
				return fileName.substring(0, pos);
			}
		}
	}

	
	/**
	 * 读取文件内容
	 * 
	 * @param fileName
	 * @param encoding
	 * @return file content
	 * @throws IOException 
	 */
	public static String readStringFile(FileInputStream fis, String encoding) throws IOException
	{
		StringBuffer sb = new StringBuffer();
		BufferedReader reader =null;
		try
		{
			reader = new BufferedReader(new InputStreamReader(fis,
					encoding));
			while (reader.ready())
			{
				String line = reader.readLine();
				sb.append(line);
				sb.append("\r\n");
			}
		}
		finally
		{
			if(reader!=null)
				reader.close();
			if(fis!=null)
				fis.close();
		}
		return sb.toString();
	}
	/**
	 * 读取文件内容
	 * 
	 * @param fileName
	 * @param encoding
	 * @return file content
	 * @throws IOException 
	 */
	public static String readStringFile(String fileName, String encoding) throws IOException
	{
		StringBuffer sb = new StringBuffer();
		FileInputStream fis = null;
		BufferedReader reader =null;
		try
		{
			fis = new FileInputStream(fileName);
			reader = new BufferedReader(new InputStreamReader(fis,
					encoding));
			while (reader.ready())
			{
				String line = reader.readLine();
				sb.append(line);
				sb.append("\r\n");
			}
		}
		finally
		{
			if(reader!=null)
				reader.close();
			if(fis!=null)
				fis.close();
		}
		return sb.toString();
	}
	
	
	/**
	 * 读取文件内容
	 * 从classpath中读取
	 * @param fileName
	 * @param encoding
	 * @return file content
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static String readStringFileInClassPath(String fileName, String encoding) throws IOException, ClassNotFoundException
	{
		StringBuffer sb = new StringBuffer();
		InputStream fis = null;
		BufferedReader reader =null;
		try
		{
//			fis = Class.forName("com.utils.FileUtils").getResourceAsStream(fileName);
			ClassLoader loader =FileUtils.class.getClassLoader();

			fis = ClassLoader.getSystemResourceAsStream(fileName); 
			reader = new BufferedReader(new InputStreamReader(fis,
					encoding));
			while (reader.ready())
			{
				String line = reader.readLine();
				sb.append(line);
				sb.append("\r\n");
			}
		}
		finally
		{
			if(reader!=null)
				reader.close();
			if(fis!=null)
				fis.close();
		}
		return sb.toString();
	}
	
	
	/**
	 * 以UTF-8读取文件内容
	 * @param fileName
	 * @param encoding
	 * @return
	 * @throws Exception
	 */
	public static String readStringFileUTF8(String fileName) throws Exception
	{
		return readStringFile(fileName, "UTF-8");
	}
	
	/**
	 * 读取一组txt文件，将文件解析成Map<[文件名], [文件内容]>
	 * 
	 * @param configPath
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> loadTxtFiles(String configPath) throws Exception
	{
		List<String> files = listDirAllFiles(configPath);
		Map<String, String> map = new HashMap<String, String>();
		for (String fileName : files)
		{
			String txtStr = readStringFileUTF8(configPath
					+ File.separator + fileName + ".txt");
			map.put(fileName, txtStr);
		}
		return map;
	}
	
	/**
	 * 加载文件成二进制
	 * @param name
	 * @return
	 * @throws IOException
	 */
	 public static byte[] loadClassData(String name)throws IOException{  
	        //读取类文件  
	        File file = new File(name);  
	        if(!file.exists()){  
	            return null;  
	        }  
	        FileInputStream input = new FileInputStream(file);  
	        long length = file.length();  
	        byte[] bt = new byte[(int)length];  
	        int rl = input.read(bt);  
	        if(rl != length){  
	            throw new IOException("不能读取所有内容");  
	        }  
	        input.close();  
	        return bt;  
	    }  
	
	public static List<String> listDirAllFiles(String root) {
		File dir = new File(root);
		root = dir.getAbsolutePath();

		List<String> fileNames = new ArrayList<String>();

		File[] files = dir.listFiles();
		for (File file : files) {
			listAllFiles(root, file, fileNames);
		}

		return fileNames;
	}

	public static String readContent(String fileName) {

		FileInputStream fis = null;
		File file = new File(fileName);
		try {
			byte[] data = new byte[(int) file.length()];

			int length = 0;
			int index = 0;
			fis = new FileInputStream(fileName);

			while ((length = fis.read(data, index, data.length - index)) > 0) {
				index += length;
			}
			// java.nio.charset.Charset.forName("GBK").newDecoder().
			String content = new String(data, "UTF-8");
			byte[] data2 = content.getBytes("UTF-8");
			if (ByteUtils.equals(data, data2)) {
				return content;
			}
			return new String(data, "GBK");

		} catch (Exception e) {

		} finally {
			ClassUtils.close(fis);
		}
		return null;
	}

	private static void listAllFiles(String root, File dir,
			List<String> fileNames) {
		if (dir.isDirectory()) {
			java.io.File[] children = dir.listFiles();
			for (File file : children) {
				listAllFiles(root, file, fileNames);
			}
		} else {
			String path = dir.getAbsolutePath();
			String fileName = path.substring(root.length() + 1);
			fileNames.add(fileName);
		}
	}

	public static List<String> listJarAllEntries(String jarFileName)
			throws IOException {
		JarFile jarFile = new JarFile(jarFileName);
		List<String> entryNames = new ArrayList<String>();
		Enumeration<JarEntry> jarEntries = jarFile.entries();
		while (jarEntries.hasMoreElements()) {
			JarEntry jarEntry = jarEntries.nextElement();
			entryNames.add(jarEntry.getName());
		}

		return entryNames;

	}

	public static String getBaseName(String fileName) {
		String[] temp = fileName.split("[/\\\\]+");
		return temp[temp.length - 1];
	}

	public static String getPrefix(String fileName) {
		String baseName = getBaseName(fileName);
		String[] temp = baseName.split("\\.");
		return temp[0];
	}

	public static String getSuffix(String fileName) {
		String baseName = getBaseName(fileName);
		if (baseName.contains(".")) {
			if (baseName.startsWith(".")) {
				return baseName;
			} else {
				String[] temp = baseName.split("\\.");
				return temp[1];
			}
		} else {
			return "";
		}
	}
	
	
	
	

	public static void main(String[] args) {
		String content = readContent("./public/test.txt");
	}
	
	
	/***
	 * 解压缩二进制文件
	 * @param data
	 * @return
	 */
	public static byte[] uncompress(byte[] data)
	{
		byte[] output = new byte[0];
		
		Inflater decompresser = new Inflater();
		
		decompresser.reset();
		
		decompresser.setInput(data);
		
		ByteArrayOutputStream o = new ByteArrayOutputStream(data.length);
		
		byte[] buf = new byte[1024];
		
		try
		{
			while(!decompresser.finished())
			{
				int i = decompresser.inflate(buf);
				o.write(buf, 0, i);
			}
			
			output = o.toByteArray();
		} 
		catch (DataFormatException e)
		{
			// TODO Auto-generated catch block
			output =  data;
			e.printStackTrace();
		}
		finally
		{
			try
			{
				o.close();
			} 
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		decompresser.end();
		return output;
	}
	
}
