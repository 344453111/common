package com.parse;

import com.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class ParseAll {

	/**
	 * 选择文件并执行解析
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	private static void chooseFile(String[] args) throws IOException, ClassNotFoundException {
		Properties _vp = new Properties();
//		_vp.put("file.resource.loader.path", "config");
//		new FileInputStream("javaModel.txt");
		String res = FileUtils.readStringFileInClassPath("com/javaModel.txt", "UTF-8");
//		String res =FileUtils.readContent("/model/javaModel.txt");
		
		String egretRes = FileUtils.readStringFileInClassPath("com/egretModel.txt", "UTF-8");
		ParseExcel.egretbeetlContent = egretRes;
		ParseExcel.javabeetlContent = res;
		String userDir = System.getProperty("user.dir");
		List<String> names = FileUtils.listDirAllFiles(new File(userDir).getAbsolutePath());
		for(String fileName:names)
		{
			if(!fileName.endsWith("xls")&&!fileName.endsWith("xlsx"))
				continue;
			ParseExcel.parse(new File(userDir+File.separatorChar+fileName));
		}
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		chooseFile(args);
	}

}
