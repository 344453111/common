package com.parse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.utils.FileUtils;

public class Parse 
{
	
	/** 选择文件并执行解析 
	 * @throws IOException 
	 * @throws ClassNotFoundException */
	private static boolean chooseFile(String[] args) throws IOException, ClassNotFoundException
	{
		Properties _vp = new Properties();
//		_vp.put("file.resource.loader.path", "config");
//		new FileInputStream("javaModel.txt");
		String res = FileUtils.readStringFileInClassPath("com/javaModel.txt", "UTF-8");
//		String res =FileUtils.readContent("/model/javaModel.txt");
		ParseExcel.javabeetlContent = res;
		
		res = FileUtils.readStringFileInClassPath("com/egretModel.txt", "UTF-8");
		ParseExcel.egretbeetlContent = res;
		
		String excelPath = "src/main/resources/config/excel";
		
		if(args!=null&&args.length>0)
		{
			excelPath = args[0];
		}
		_vp.put("file.resource.loader.path", excelPath);
		
		
		JFileChooser chooser = new JFileChooser();
		String userDir = System.getProperty("user.dir");
		chooser.setCurrentDirectory(new File(userDir));
		// 只选择excel文件
		chooser.addChoosableFileFilter(new FileFilter()
		{
			public String getDescription()
			{
				return null;
			} 

			public boolean accept(File file)
			{
				if (file.isDirectory())
				{
					return true;
				}
				if (file.getName().endsWith(".xls"))
				{
					return true;
				}
				if (file.getName().endsWith(".xlsx"))
				{
					return true;
				}
				if (file.getName().endsWith("ini.xml"))
				{
					return true;
				}
				return false;
			}
		});
		int result = chooser.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION)
		{
			File file = chooser.getSelectedFile();
			ParseExcel.parse(file);
			
			return true;
		}
		return false;
	}
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		chooseFile(args);
	}

}
