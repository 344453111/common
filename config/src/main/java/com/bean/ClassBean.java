package com.bean;

import java.util.ArrayList;
import java.util.List;

public class ClassBean {
	
	public String desc;
	
	public String name;
	
	public List<FieldsBean> fieldList = new ArrayList<FieldsBean>();
	
	public String fileName;
	
	public String fatherName;
	
	public String fatherPackage;
	
	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public String getName() {
		return name;
	}


	public String getFatherPackage() {
		return fatherPackage;
	}


	public void setFatherPackage(String fatherPackage) {
		this.fatherPackage = fatherPackage;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<FieldsBean> getFieldList() {
		return fieldList;
	}

	
	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFatherName() {
		return fatherName;
	}


	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}


	public void setFieldList(List<FieldsBean> fieldList) {
		this.fieldList = fieldList;
	}
}
