用于游戏服务器热修改，与项目代码解耦，任何游戏服务器都可以使用。 基于javaagent+compiler实现。不会产生额外内存,classloader重载的类需要自己扩展。 实用场景:游戏服务器在内测或公测时会因为代码有问题导致bug，用此项目可以在服务器不停机的状态下修改方法中的代码，达到修复bug，让玩家不离线继续游戏 使用方式:请见工程内 使用方式.txt 欢迎加QQ526167774 讨论

使用方式:
1,将该包打成jar
2,在需要重载的项目启动参数增加-javaagent:hotagent.jar  ,例如：    java -javaagent:hotagent.jar -jar hotagent.jar TestMainInJar 
3,在该包的MANIFEST.MF内部增加一些参数,
Can-Redefine-Classes: true
Can-Retransform-Classes: true
Can-Set-Native-Method-Prefix: true
Premain-Class: test.Premain


例如 ：
Manifest-Version: 1.0
Can-Redefine-Classes: true
Can-Retransform-Classes: true
Can-Set-Native-Method-Prefix: true
Class-Path: .
Premain-Class: test.Premain
Main-Class: test.TestMainInJar

4,启动后在自动创建reload包，需要重载的类放入该目录下