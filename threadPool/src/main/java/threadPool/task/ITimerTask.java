package threadPool.task;

import java.util.concurrent.ScheduledFuture;

public interface ITimerTask extends Runnable {
	
	void setFuture(ScheduledFuture<?> future);
	
	boolean isOver();

	long getIntervalMill();

}
