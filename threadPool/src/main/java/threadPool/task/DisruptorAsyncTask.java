package threadPool.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 异步任务
 * @author jinmiao
 * 2014-9-12 上午10:45:06
 */
public abstract class DisruptorAsyncTask implements ITask
{

	protected static final Logger logger = LoggerFactory.getLogger(DisruptorAsyncTask.class);
	
	protected Throwable e;
	

	public void execute(){
		try {
			async();
		} catch (Throwable e)
		{
			this.e = e;
			//e.printStackTrace();
			if (logger.isErrorEnabled()) {
				logger.error("Error", "#.DisruptorAsyncTask.execute", "param",e);
			}
		}
	}

	/**
	 * 异步调用
	 */
	protected abstract void async() throws Exception;
}
