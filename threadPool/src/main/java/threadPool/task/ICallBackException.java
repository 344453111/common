package threadPool.task;

/**
 * Created by JinMiao
 * 2018/5/8.
 */
public interface ICallBackException {

    void handException(Throwable throwable);
}
