package threadPool.task;

/**
 * Created by JinMiao
 * 2018/5/3.
 */
public interface IAsyncTask<T> {
    T async();
}
