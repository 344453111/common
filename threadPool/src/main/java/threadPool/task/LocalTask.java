package threadPool.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import threadPool.thread.DisruptorThread;
import threadPool.thread.IMessageExecutor;

/**
 * Created by JinMiao
 * 2018/5/8.
 */
public abstract class LocalTask implements ITask{

    protected static final Logger logger = LoggerFactory.getLogger(LocalTask.class);

    protected IMessageExecutor messageExecutor;

    public LocalTask() {
        Thread currentThread = Thread.currentThread();
        if(!(currentThread instanceof DisruptorThread)){
            logger.error("current is not DisruptorThread");
            return;
        }
        this.messageExecutor = ((DisruptorThread)currentThread).getMessageExecutor();
    }

}
