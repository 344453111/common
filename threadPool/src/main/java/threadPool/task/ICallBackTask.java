package threadPool.task;

public interface ICallBackTask<T> {
	void callBack(T t);
}
