package threadPool.task;

/**
 * Created by JinMiao
 * 2018/5/8.
 */
public abstract class OutTask implements ITask{

    public abstract String getExecuteName();

}
