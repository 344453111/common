package threadPool.thread;

import com.lmax.disruptor.EventHandler;

public class DistriptorEventHandler implements EventHandler<DistriptorHandler>{

	public void onEvent(DistriptorHandler event, long sequence,
			boolean endOfBatch) throws Exception 
	{
		event.execute();
	}
}
