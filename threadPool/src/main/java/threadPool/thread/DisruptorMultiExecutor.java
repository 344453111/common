package threadPool.thread;

import threadPool.task.ITask;

import java.util.concurrent.atomic.AtomicInteger;
/**
 * 基于{@link #Disruptor} 的多线程实现
 * @author jinmiao
 * 2014-9-12 下午2:19:54
 */
public class DisruptorMultiExecutor implements IMessageExecutor {

	private DisruptorSingleExecutor[] processArray;
	/**线程数量**/
	private int threadNum;

	private String threadName;
	
	private AtomicInteger index = new AtomicInteger();

	
	public DisruptorMultiExecutor(String threadName,int threadNum)
	{
		this.threadNum = threadNum;
		this.threadName =threadName;
	}
	
	
	public void start()
	{
		processArray = new DisruptorSingleExecutor[threadNum];
		for(int i =0;i<threadNum;i++)
		{
			DisruptorSingleExecutor process = new DisruptorSingleExecutor(threadName+i);
			processArray[i] = process;
			process.start();
		}
	}

	public void stop() {
		for(DisruptorSingleExecutor process:processArray)
		{
			process.stop();
		}
	}


	public boolean isFull() {
		return false;
	}


	@Override
	public void execute(ITask iTask) {
		int index = this.index.incrementAndGet();
		DisruptorSingleExecutor process  = processArray[index%threadNum];
		process.execute(iTask);
	}

}
