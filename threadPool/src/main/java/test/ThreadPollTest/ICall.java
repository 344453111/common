package test.ThreadPollTest;

import java.util.concurrent.CompletableFuture;

public interface ICall {
	public CompletableFuture<String> asyncCall(int a,int b);
	
	public String syncCall(int a,int b);
	
	public CompletableFuture<String> waitSyncCall(int a,int b);
}
