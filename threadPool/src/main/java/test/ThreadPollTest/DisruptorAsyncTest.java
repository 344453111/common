package test.ThreadPollTest;

import threadPool.thread.BaseDisruptorExecutorPool;
import threadPool.thread.DisruptorMultiExecutor;
import threadPool.thread.DisruptorSingleExecutor;

/**
 * Created by JinMiao
 * 2018/5/3.
 */
public class DisruptorAsyncTest {
    public static void main(String[] args) {
        DisruptorSingleExecutor disruptorSingleExecutor1 = new DisruptorSingleExecutor("1");
        DisruptorSingleExecutor disruptorSingleExecutor2 = new DisruptorSingleExecutor("2");
        DisruptorMultiExecutor disruptorMultiExecutor = new DisruptorMultiExecutor("mutil",10);
        disruptorSingleExecutor1.start();
        disruptorSingleExecutor2.start();
        disruptorMultiExecutor.start();

        //disruptorSingleExecutor1.execute(() -> {
        //    DisruptorAsyncBackTask disruptorAsyncBackTask = new DisruptorAsyncBackTask() {
        //        @Override
        //        protected void onFail(Throwable e) {
        //            System.out.println("fail"+Thread.currentThread().getName());
        //        }
        //
        //        @Override
        //        protected void doCallBack() {
        //            System.out.println("back"+Thread.currentThread().getName());
        //        }
        //
        //        @Override
        //        protected void async() throws Exception {
        //            System.out.println("async"+Thread.currentThread().getName());
        //            throw new RuntimeException("");
        //        }
        //    };
        //    disruptorMultiExecutor.execute(disruptorAsyncBackTask);
        //});
        BaseDisruptorExecutorPool baseDisruptorExecutorPool = new BaseDisruptorExecutorPool(){
            public BaseDisruptorExecutorPool init(){
                createDisruptorProcessor("process",2);
                return this;
            }
        }.init();

        //lambda回调函数
        disruptorSingleExecutor1.execute(() ->
                baseDisruptorExecutorPool.call("process",() -> {
                    System.out.println("async"+Thread.currentThread().getName());
                    //return "name";
                    throw  new RuntimeException();
                },
                s -> {
                    System.out.println("callback"+Thread.currentThread().getName());
                    System.out.println(s);
                },throwable -> {
                    System.out.println(throwable);
                }
            )
        );
    }
}
