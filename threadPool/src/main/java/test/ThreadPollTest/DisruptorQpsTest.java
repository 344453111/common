package test.ThreadPollTest;

import threadPool.thread.BaseDisruptorExecutorPool;
import threadPool.thread.DisruptorSingleExecutor;

import java.util.concurrent.CompletableFuture;

/**
 * 测试disruptor性能
 * i7 q720 600W qps
 * e3 1230v2  1000W qps
 * @author jinmiao
 * 2016年7月6日
 *
 */
public class DisruptorQpsTest extends BaseDisruptorExecutorPool{
	
	static int in = 0;
	static long start = System.currentTimeMillis();
	static long end = System.currentTimeMillis();
	private static CompletableFuture<Void> thenAcceptAsync;
	public static void main(String[] args) throws InterruptedException {
		DisruptorSingleExecutor t = new DisruptorSingleExecutor("gaga");
		
		t.start();
		
		CompletableFuture<String> future = new CompletableFuture<>();
		
		Thread t1 = new Thread(()-> t.execute(()->{
			if((in++)%10000000==0)
			{
				end = System.currentTimeMillis();
				System.out.println(end-start);
				start = end;
				System.out.println(Thread.currentThread().getName()+"3");
			}
		}));
		Thread t2 = new Thread(()->{t.execute(()->{
			if((in++)%10000000==0)
			{
				end = System.currentTimeMillis();
				System.out.println(end-start);
				start = end;
			}
		});});
		Thread t3 = new Thread(()->{t.execute(()->{
			if((in++)%10000000==0)
			{
				end = System.currentTimeMillis();
				System.out.println(end-start);
				start = end;
				System.out.println(Thread.currentThread().getName()+"2");				
			}
		});});
		
		
		
//		
		t1.start();
		t2.start();
//		t3.start();
		
//		ICall call = null;
//		//异步 
//		call.asyncCall(1, 2).thenAcceptAsync(s->{System.out.println(s);}, t);
//		//同步调用
//		String str = call.syncCall(1, 2);
//		//同步延时调用
//		try {
//			call.waitSyncCall(1, 2).get();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		}
		
//		future.thenAcceptAsync(s->{
//		System.out.println("aaa"+s.hashCode());
//		System.out.println(Thread.currentThread().getName())
//		;}, t);
		
		
		
		for(;;)
		{
			t.execute(()->{
				if((in++)%10000000==0)
				{
					end = System.currentTimeMillis();
					System.out.println(end-start);
					start = end;
//					System.out.println(Thread.currentThread().getName()+"1");
//					future.complete("55");
				}
			});
		}
	}
	
}
