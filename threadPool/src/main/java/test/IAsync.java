package test;

public interface IAsync<T> {

	T async();

}
