package test;

public class Test {
	public <T> void asyncTask(IAsync<T> async ,ICallBack<T> back,Class<T> cls)
	{
		T t = async.async();
		back.callBack(t);
	}
}
