package lock;

/**
 * Redis 参数
 */
public class RedisConfig {

	private String host;
	private int port;
	private int timeout;
	private String password;
	private int database;
	private int maxIdle;
	private int maxTotal;
	private int maxWaitMillis;
	private boolean testOnBorrow;

	// Redis Lock
	private int expireSeconds = 300; // 默认过期时间
	private int retrySeconds = 2; // 默认重试时间
	private String lockPrefix = "lock_";

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getDatabase() {
		return database;
	}

	public void setDatabase(int database) {
		this.database = database;
	}

	public int getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}

	public int getMaxTotal() {
		return maxTotal;
	}

	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	public int getMaxWaitMillis() {
		return maxWaitMillis;
	}

	public void setMaxWaitMillis(int maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
	}

	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}

	public void setTestOnBorrow(boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}

	public int getExpireSeconds() {
		return expireSeconds;
	}

	public void setExpireSeconds(int expireSeconds) {
		this.expireSeconds = expireSeconds;
	}

	public int getRetrySeconds() {
		return retrySeconds;
	}

	public void setRetrySeconds(int retrySeconds) {
		this.retrySeconds = retrySeconds;
	}

	public String getLockPrefix() {
		return lockPrefix;
	}

	public void setLockPrefix(String lockPrefix) {
		this.lockPrefix = lockPrefix;
	}

}
