package lock;

import redis.clients.jedis.Jedis;

public interface RedisCallback<T> {

	T call(Jedis jedis) throws Exception;

}
