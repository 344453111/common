package lock;

public enum RedisKey {

	CASHIER, DEPOSIT;

	public String[] getKeys(Object... suffixs) {
		String[] keys = new String[suffixs.length];
		for (int i = 0; i < keys.length; i++) {
			keys[i] = this.name().toLowerCase() + "_" + suffixs[i];
		}
		return keys;
	}

}
