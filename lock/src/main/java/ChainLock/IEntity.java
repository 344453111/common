package ChainLock;
/**
 * 实体标识接口
 */
@SuppressWarnings("rawtypes")
public interface IEntity<T extends Comparable> {
    /**
     * 获取实体标识
     * 
     * @return
     */
    public T getIdentity();
}